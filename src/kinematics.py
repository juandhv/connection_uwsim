#!/usr/bin/env python
"""
Created on March 5, 2018

@author: juandhv (Juan David Hernandez Vega, juandhv@rice.edu)
"""

import roslib; roslib.load_manifest('connection_uwsim')
import rospy
from std_msgs.msg import String
from nav_msgs.msg import Odometry

from sensor_msgs.msg import PointCloud2, PointField, LaserScan
import tf
import numpy as np

from math import radians

"""
Publish Position and Orientation for UWSim
"""
def publishPosOriUWSim():
    rospy.loginfo("+++ Publish Position and Orientation for UWSim +++")
        
    pub_octomap = rospy.Publisher('/pose_ekf_slam/odometry', Odometry, queue_size = 1)
    
    odom_msg = Odometry()
    
    odom_msg.pose.pose.orientation.w = 1.0
    
    delta_t = 1.0/10.0
    rospy_rate = rospy.Rate(1/delta_t)
    
    #http://planning.cs.uiuc.edu/node660.html
    u = 0.5
    w = 0.1
    
    x = 0.0
    y = 0.0
    theta = 0.0
    
    while not rospy.is_shutdown():
		vx = u*np.cos(theta)
		vy = u*np.sin(theta)
		vtheta = w
		
		#numerical integration
		x = x + delta_t*vx
		y = y + delta_t*vy
		theta = theta + delta_t*vtheta
		
		odom_msg.pose.pose.position.x = x
		odom_msg.pose.pose.position.y = y
		
		quaternion = tf.transformations.quaternion_from_euler(0.0, 0.0, theta)
		odom_msg.pose.pose.orientation.x = quaternion[0]
		odom_msg.pose.pose.orientation.y = quaternion[1]
		odom_msg.pose.pose.orientation.z = quaternion[2]
		odom_msg.pose.pose.orientation.w = quaternion[3]
		
		odom_msg.header.stamp = rospy.Time.now()
		pub_octomap.publish(odom_msg)
		rospy_rate.sleep()
   
    return pub_octomap

if __name__ == '__main__':
    try:
        rospy.init_node('kinematics')
        publishPosOriUWSim()
    except rospy.ROSInterruptException:
        pass
